package producerandconsumer;

import java.util.Random;
import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable{
    private BlockingQueue<PCData> queue;
    private int consumerId;
    public Consumer(BlockingQueue<PCData> queue,int consumerId){
        this.queue = queue;
        this.consumerId = consumerId;
    }
    @Override
    public void run() {
        System.out.println("start ConsumerId:"+consumerId);
        Random r = new Random();
        try {
            while(true){
                Thread.sleep(200);
                PCData data = queue.take();
                if(data != null){
                    System.out.println("消费者"+consumerId+"--消费了"+data.getName());
                    Thread.sleep(r.nextInt(1000));
                }
            }
        }catch (InterruptedException e){
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }
    }
}
