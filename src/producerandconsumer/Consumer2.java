package producerandconsumer;

import lombok.SneakyThrows;

public class Consumer2 implements Runnable{
    private PCData2 pcData2;
    public Consumer2(PCData2 pcData2){
        this.pcData2 = pcData2;
    }

    @Override
    public void run() {
        while(true){
            if(Thread.currentThread().isInterrupted()){
                break;
            }
            try {
                Thread.sleep(1000);
                pcData2.remove();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
