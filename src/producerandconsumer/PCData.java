package producerandconsumer;

/**
 * 生产消费的货物信息
 */

public class PCData {
    private int id;
    private String name;

    public PCData() {
    }

    public PCData(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
