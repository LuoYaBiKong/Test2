package producerandconsumer;


public class Producer3 implements Runnable {
    private PCData3 pcData3;
    public Producer3(PCData3 pcData3){
        this.pcData3 = pcData3;
    }
    @Override
    public void run() {
        while(true){
            try{
                Thread.sleep((long)(1000*Math.random()));
            }catch (Exception e){
                e.printStackTrace();
            }
            pcData3.add();
        }
    }
}
