package producerandconsumer;

import lombok.SneakyThrows;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class Producer2 implements Runnable{
    private PCData2 pcData2;

    public Producer2(PCData2 pcData2){
        this.pcData2 = pcData2;
    }
    @Override
    public void run() {
        while(true){
            if(Thread.currentThread().isInterrupted()){
                break;
            }
            try {
                Thread.sleep(1000);
                pcData2.add();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
