package producerandconsumer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Test2 {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newCachedThreadPool();
        PCData2 pcData2 = new PCData2();
        Producer2 p1 = new Producer2(pcData2);
        Producer2 p2 = new Producer2(pcData2);
        Producer2 p3 = new Producer2(pcData2);
        Consumer2 c1 = new Consumer2(pcData2);
        executorService.execute(p1);
        executorService.execute(p2);
        executorService.execute(p3);
        executorService.execute(c1);

    }
}
