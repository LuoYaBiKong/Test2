package producerandconsumer;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;

public class Test {
    public static void main(String[] args) throws InterruptedException {
        BlockingQueue<PCData> queue = new LinkedBlockingDeque<>(10);
        Producer p1 = new Producer(queue,1);
        Producer p2 = new Producer(queue,2);
        Producer p3 = new Producer(queue,3);
        Consumer c1 = new Consumer(queue,1);
        Consumer c2 = new Consumer(queue,2);
        Consumer c3 = new Consumer(queue,3);
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.execute(p1);
        executorService.execute(p2);
        executorService.execute(p3);
        Thread.sleep(2000);
        executorService.execute(c1);
        executorService.execute(c2);
        executorService.execute(c3);
        Thread.sleep(2000);
        p1.stop();
        p2.stop();
        p3.stop();
        Thread.sleep(1000);
        System.out.println("生产者停止生产");
        Thread.sleep(1000);
        System.out.println("消费者停止消费");
        executorService.shutdown();
    }
}
