package producerandconsumer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Test3 {
    public static void main(String[] args) {
        Lock lock = new ReentrantLock();
        Condition producerCondition = lock.newCondition();
        Condition consumerCondition = lock.newCondition();
        ExecutorService executorService = Executors.newCachedThreadPool();
        PCData3 pcData3 = new PCData3(lock,producerCondition,consumerCondition);
        executorService.execute(new Producer3(pcData3));
        executorService.execute(new Producer3(pcData3));
        executorService.execute(new Producer3(pcData3));
        executorService.execute(new Consumer3(pcData3));
    }
}
