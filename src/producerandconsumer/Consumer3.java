package producerandconsumer;

public class Consumer3 implements Runnable{
    private PCData3 pcData3;
    public Consumer3(PCData3 pcData3){
        this.pcData3 = pcData3;
    }
    @Override
    public void run() {
        try{
            while(true){
                Thread.sleep((long)(1000*Math.random()));
                pcData3.remove();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
