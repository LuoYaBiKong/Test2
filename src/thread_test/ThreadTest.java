package thread_test;

import org.openjdk.jol.info.ClassLayout;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.LockSupport;
import java.util.concurrent.locks.ReentrantLock;

import static java.lang.Thread.currentThread;
import static java.lang.Thread.sleep;

public class ThreadTest {
    public static Object lock = new Object();
    public static void main(String[] args) throws InterruptedException {
//        System.out.println(ClassLayout.parseInstance(lock).toPrintable());
        Lock lock = new ReentrantLock();
        Condition condition = lock.newCondition();
        Thread t1 = new Thread(new MyThread());
        Thread t2 = new Thread(new MyThread());
        Thread t3 = new Thread(new MyThread());
//        t3.setPriority(10);
        System.out.println("主线程中准备开启线程t1和线程t2");
        t1.start();
        t3.start();
//        //下面的代码即为join方法，join方法的作用是使主线程等待，等到执行线程执行完成再唤醒，主线程等待的过程中并不会影响其他已运行的线程
//        synchronized (t1){
//            long millis = 0;//这个值相当于join传参
//            long base = System.currentTimeMillis();
//            long now = 0;
//
//            if (millis < 0) {
//                throw new IllegalArgumentException("timeout value is negative");
//            }
//
//            if (millis == 0) {
//                while (t1.isAlive()) {
//                    t1.wait(0);
//                }
//            } else {
//                while (t1.isAlive()) {
//                    long delay = millis - now;
//                    if (delay <= 0) {
//                        break;
//                    }
//                    t1.wait(delay);
//                    now = System.currentTimeMillis() - base;
//                }
//            }
//        }
        t1.join();
        t2.start();
        System.out.println("主线程执行结束");
    }

    public static class MyThread implements Runnable{

        @Override
        public void run() {
            for (int i = 0; i < 5; i++) {
                try {
                    sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName()+"--"+i);
            }
        }
    }
}
