package juc;

import java.util.concurrent.Semaphore;

public class SemaphoreDemo {
    private final static int NUMS = 5;
    private final static Semaphore semaphore = new Semaphore(NUMS);
    public static void main(String[] args) {
        for (int j = 0; j < NUMS*2; j++) {
            new Thread(new SemaohoreRunning(semaphore, NUMS)).start();
        }
    }

    static class SemaohoreRunning implements Runnable{
        private int i;
        private Semaphore semaphore;
        public SemaohoreRunning(Semaphore semaphore,int i){
            this.semaphore = semaphore;
            this.i = i;
        }
        @Override
        public void run() {
            try {
                semaphore.acquire();
                Thread.sleep(2000);
                System.out.println(Thread.currentThread().getName()+"执行完毕！");
                semaphore.release();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
