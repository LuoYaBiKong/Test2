import java.util.Arrays;

public class Sort {

    public static void main(String[] args) {
        int[] array = new int[]{5,4,3,2,1};
        qucikSort(array, 0, array.length-1);
        System.out.println(Arrays.toString(array));
    }

    public static void qucikSort(int[] array, int left, int right){
        int benchmarkNo;
        if(left < right){
            benchmarkNo = partition(array, left, right);
            //递归分治
            //左部分快排
            qucikSort(array, left,benchmarkNo-1);
            //右部分快排
            qucikSort(array,benchmarkNo+1, right);
        }
    }

    private  static int partition(int[] array, int left, int right){
        int benchmarkNo = array[left];
        while(left < right){
            while (left < right && array[right]>= benchmarkNo){
                right--;
            }
            if(left < right){
                array[left] = array[right];
                left++;
            }
            while(left < right && array[left] <= benchmarkNo){
                left++;
            }
            if(left < right){
                array[right] = array[left];
                right--;
            }
        }
        array[left] = benchmarkNo;
        return left;
    }
}
