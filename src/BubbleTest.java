import java.util.Arrays;

public class BubbleTest {

    public static void main(String[] args) {
        int [] source = new int [100000];
        for(int i=2;i<100000;i++){
            source[i] = i;
        }
        source[0] = 1;
        source[1] = 100000;
        System.out.println(Arrays.toString(source));
        long l1 = System.currentTimeMillis();
        source = bubbleSort(source);
        long l2 = System.currentTimeMillis();
        System.out.println(Arrays.toString(source));
        System.out.println(l2-l1);
//        long l3 = System.currentTimeMillis();
//        source = bubbleSort1(source);
//        long l4 = System.currentTimeMillis();
//        System.out.println(Arrays.toString(source));
//        System.out.println(l4-l3);
    }

    public static int [] bubbleSort(int [] source){
        for(int i = 0; i<source.length-1;i++){
            for(int j=0;j<source.length-i-1;j++){
                int a = 0;
                if(source[j] > source[j+1]){
                    a = source[j];
                    source[j] = source[j+1];
                    source[j+1] = a;
                }
            }
        }
        return source;
    }

    public static int [] bubbleSort1(int [] source){
        for(int i = 0; i<source.length-1;i++){
            boolean flag = false;
            for(int j=0;j<source.length-i-1;j++){
                int a = 0;
                if(source[j] > source[j+1]){
                    a = source[j];
                source[j] = source[j+1];
                source[j+1] = a;
                flag = true;
            }
            }
            if(!flag){
                break;
            }
        }
        return source;
    }
}
