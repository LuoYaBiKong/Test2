import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.*;

public class AQSSourceTest {
//    public int i = 0;
    public static AtomicInteger j = new AtomicInteger(0);
    public static void main(String[] args) throws InterruptedException {
        AQSSourceTest test = new AQSSourceTest();
        ExecutorService executorService =new ThreadPoolExecutor(1,
                5,
                2,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(10),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.DiscardPolicy());
        for (int i = 0; i < 20; i++) {
            executorService.submit(new FutureTask<String>(new Callable<String>() {
                @Override
                public String call() throws Exception {
                    System.out.println(Thread.currentThread().getName()+"开始执行了！"+j.addAndGet(1));
                    Thread.sleep(1000);
                    System.out.println(Thread.currentThread().getName()+"执行结束了！"+j);
                    return "Success";
                }
            }));
        }
        executorService.shutdown();

//        while(test.i.get() <20){
//            test.test2();
//        }
//        while(test.i < 20){
//            new Thread(() ->{
//                test.i++;
//                System.out.println(Thread.currentThread().getName()+"--**--"+test.i);
//        }).start();
//        }
        //t1线程先被park，后unpark，得到正常执行
        //t2线程先unpark，后park，亦得到正常执行
//        Thread t1 = new Thread(new RunningTest());
//        Thread t2 = new Thread(new RunningTest());
//        t1.start();
//        TimeUnit.MILLISECONDS.sleep(5000);
//        t2.start();
//        LockSupport.unpark(t1);
//        LockSupport.unpark(t2);
//        //这种情况会出现死锁
//        Thread t3 = new Thread(new RunningTest());
//        LockSupport.unpark(t3);
//        t3.start();

//        while(test.i <20){
//            test.test2();
//        }
    }

//    public void test(){
//        i++;
//        System.out.println(i);
//    }

//    public synchronized void test1(){
//        new Thread(() -> {
//            i++;
//            System.out.println(i);
//        }).start();
//    }

//    public void test2(){
//        ReentrantLock lock = new ReentrantLock();
//        lock.lock();
//        try {
//            new Thread(() -> {
//                i++;
////                i.getAndIncrement();
//                System.out.println(Thread.currentThread().getName()+"---"+i);
//            }).start();
//        }catch (Exception e){
//            e.printStackTrace();
//        }finally {
//            lock.unlock();
//        }
//    }

    static class RunningTest implements Runnable{
        @Override
        public void run() {
            System.out.println(Thread.currentThread().getName()+"程序开始执行");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            LockSupport.park();
            System.out.println(Thread.currentThread().getName()+"程序执行结束");
        }
    }
}
