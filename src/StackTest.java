import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;

public class StackTest {
    public static void main(String[] args) {
        StackTest st = new StackTest();
        String abbaca = st.removeDuplicates("abbaca");
        System.out.println(abbaca);
    }
    //给出由小写字母组成的字符串 S，重复项删除操作会选择两个相邻且相同的字母，并删除它们。
    //在 S 上反复执行重复项删除操作，直到无法继续删除。
    //在完成所有重复项删除操作后返回最终的字符串。答案保证唯一。
    //
    //示例：
    //输入："abbaca"
    //输出："ca"
    //
    //解释：
    //例如，在 "abbaca" 中，我们可以删除 "bb" 由于两字母相邻且相同，这是此时唯一可以执行删除操作的重复项。之后我们得到字符串 "aaca"，其中又只有 "aa" 可以执行重复项删除操作，所以最后的字符串为 "ca"。
    //提示：
    //  1 <= S.length <= 20000
    //  S 仅由小写英文字母组成。
    //---------------------------------------
    //优化：
    //通过StringBuffer构建栈结构实现
    public String removeDuplicates(String S) {
        char[] ch = S.toCharArray();
        Deque stack = new ArrayDeque();
        for(int i=0;i<ch.length;i++){
            if(stack.isEmpty()){
                stack.push(ch[i]);
            }else{
                char peek = (char)stack.peek();
                if(peek == ch[i]){
                    stack.poll();
                }else {
                    stack.push(ch[i]);
                }
            }
        }
        StringBuffer sb = new StringBuffer();
        while(!stack.isEmpty()){
            sb.insert(0, stack.poll());
        }
        return sb.toString();
    }
}
