import java.util.ArrayList;
import java.util.List;

public class PrimeNumberTest {

    public static void main(String[] args) {
        int number = 1000000001;
        long l1 = System.currentTimeMillis();
        boolean flag = isPrimeNumber(number);
        long l2 = System.currentTimeMillis();
        System.out.println(flag);
        System.out.println(l2-l1);
        List<Integer> list = getPrimeNumber(100);
        System.out.println(list.size());
        System.out.println(list);
    }

    public static boolean isPrimeNumber(int number){
        if(number<=1){
            return false;
        }
        if(number%2 == 0){
            return false;
        }
        int count = 0;
        for(int i= 1;i<=number;i++){
            if(number%i == 0){
//                System.out.println(i);
                count++;
                if(count >2){
                    return false;
                }
            }
        }
        if(count == 2){
            return true;
        }
        return false;
    }
    public static List<Integer> getPrimeNumber(int sourceNumber){
        List<Integer> list = new ArrayList<>();
        for(int i=1;i<=sourceNumber;i++){
            boolean flag = isPrimeNumber(i);
            if(flag){
                list.add(i);
            }
        }
        return list;
    }
}
