public class DataStructureTest {
    public static void main(String[] args) {
        int LEN = 10000;
        int[][] arr = new int[LEN][LEN];
        long l1 = System.currentTimeMillis();
        for (int i = 0; i < LEN; i++) {
            for (int j = 0; j < LEN; j++) {
                arr[i][j] = 1;
            }
        }
        long l2 = System.currentTimeMillis();
        System.out.println(l2-l1);
        long l3 = System.currentTimeMillis();
        for (int i = 0; i < LEN; i++) {
            for (int j = 0; j < LEN; j++) {
                arr[j][i] = 1;
            }
        }
        long l4 = System.currentTimeMillis();
        System.out.println(l4-l3);
    }
}
