import com.sun.deploy.util.StringUtils;

import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {

       /* System.out.println("Hello World!");
        String str = "2018-01";
        String regexp = "(\\d{4})-(([0]\\d{1})|[1][0-2])";
        //String regexp = "\\d{4}-\\d{2}";
        String str1 = "08";
        //String regexp = "([0]\\d{1})|[1][0-2]";
        Matcher m = Pattern.compile(regexp).matcher(str);
        while(m.find()){
            System.out.println(m.group());
        }*/

//       List<String> list = new ArrayList<String>(){{
//           add("a");
//           add("b");
//           add("cc");
//           add("d");
//           add("e");
//       }};
//        System.out.println(list);
//        System.out.println(list.stream().filter(a -> !a.equals("cc")).collect(Collectors.toList()));
//        System.out.println(list);

//        List<String> list = new ArrayList<>();
//       list.add("");
//       list.add("");
//       list.add("");
//       list.add("a");
//       list.add("b");
//       list.add("c");
//       list.add("");
//       list.add("d");
//       list.add("e");
//       list.add("f");
//       list.forEach(System.out::println);
//       String ss = String.join(",",list);
//        System.out.println(ss);
//       String sss = list.toString();
//        System.out.println(sss);
//        List<String> list1 = new ArrayList<>();
//        list1.add("a");
//        list1.add("c");
//        list1.add("");
//        List<String> intersection = list.stream().filter(item -> list1.contains(item)).collect(toList());
//        System.out.println("---交集 intersection---");
//        intersection.parallelStream().forEach(System.out :: println);
//       //以下代码用来去除结合中前x项为空的元素
//       Iterator it = list.iterator();
//       int count = 0;
//       while (it.hasNext()){
//           String s = (String)it.next();
//           if("".equals(s)||s == null){
//                it.remove();
//           }else{
//               count = 1;
//           }
//           if(count > 0){
//               break;
//           }
//       }
//
////       list = list.subList(0,3);
////        Collections.shuffle(list);
//       System.out.println(list);
       /*List<List<Integer>> list = new ArrayList<>();
       List<Integer> iList1 = new ArrayList<>();
       List<Integer> iList2 = new ArrayList<>();
       iList1.add(1);
       iList1.add(2);
       iList1.add(3);
       iList2.add(4);
       iList2.add(5);
       iList2.add(6);
       list.add(iList1);
       list.add(iList2);
        int [] sum = new int [iList1.size()];
       for(int i=0;i<list.size();i++){
           for(int j=0;j<iList1.size();j++){
                sum[j] += list.get(i).get(j);
           }
       }
       System.out.println(Arrays.toString(sum));*/
       /*List<Date> list = new ArrayList<>();
       Date minDate = Collections.min(list);
       System.out.println(minDate);*/
//反向迭代测试
//        List<String> list = new ArrayList<>();
//        for (int i=0;i<5;i++){
//            list.add("第"+i+"项");
//        }
//        //list.add(0,"添加项");
//        for(int i=3;i>0;i--){
//            list.add(0,"添加"+i+"项");
//        }
//        System.out.println(list);
//        ListIterator lit = list.listIterator();
//        while(lit.hasNext()){
//            System.out.println(lit.next());
//
//        }
//        Map集合遍历
//        Map<String, Integer> map = new HashMap<>();
//        map.put("a", 1);
//        map.put("b", 2);
//        map.put("c", 3);
//        map.put("d", 4);
//        map.put("e", 5);
//        map.put("f", 6);
//        for (Map.Entry<String, Integer> entry : map.entrySet()) {
//            String key = entry.getKey();
//            Integer value = entry.getValue();
//            System.out.println("键：" + key + "  值：" + value);
//
//        }
//        // 该方法性能略低于上面的方法
//        map.forEach((key,value) -> {
//            System.out.println(key+":"+value);
//        });
        // 随机数测试
//        Random r = new Random();
//        int intr = r.nextInt();
//        double doubler = r.nextDouble();
//        double mr = Math.random();
//        System.out.println(intr+"----"+doubler+"----"+mr);
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
//        Date date = null;
//        try{
//            date = sdf.parse("2017");
//            System.out.println(date);
//        }catch(Exception e){
//            e.printStackTrace();
//        }
//        String a = "2017";
//        String aa = String.valueOf(Integer.parseInt(a)-2);
//        System.out.println(aa);
//        Map<String,String> map = new HashMap<>();
//        map.put("1",null);
//        System.out.println(map);
//        Double d = null;
//        System.out.println(Double.isNaN(d));
        //集合按照指定顺序排序
//        String [] defined = {"大庆","吉林","辽河","华北","冀东"};
//        List<String> definedOrder =Arrays.asList(defined);
//        String [] toBeOrdered = {"辽河","大庆","华北","冀东","吉林"};
//        List<String> toBeOrderedList = Arrays.asList(toBeOrdered);
//        Collections.sort(toBeOrderedList, new Comparator<String>() {
//            @Override
//            public int compare(String o1, String o2) {
//                int io1 = definedOrder.indexOf(o1);
//                int io2 = definedOrder.indexOf(o2);
//                return io1-io2;
//            }
//        });
//        System.out.println(toBeOrderedList);
//        String s1 = "nihao123!";
////        String s2 = "你好123！";
////        String ss1 = s1.toUpperCase();
////        String ss2 = s2.toUpperCase();
////        System.out.println(ss1+"---------"+ss2);
//        String s1 = "ABCABC";
//        String s2 = "A";
//        String[] split = s1.split(s2);
//        boolean contains = s1.contains(s2);
//        char[] chars = s2.toCharArray();
//        System.out.println(Arrays.toString(split));
//        System.out.println(contains);
//        System.out.println(Arrays.toString(chars));
//-------------------------------------------------------------------------------------------------------
//        String a = deleteE("兰8,兰9,兰8-5X,兰8-4X", "兰8-5X", ",|，|、");
//        System.out.println(a);
//-------------------------------------------------------------------------------------------------------
//        int i1 = 100;
//        int i2 = 100;
//        int i3 = 1000;
//        int i4 = 1000;
//        Integer l1 = 100;
//        Integer l2 = 100;
//        Integer l3 = 1000;
//        Integer l4 = 1000;
//        System.out.println(i1 == i2);
//        System.out.println(i3 == i4);
//        System.out.println(l1 == l2);
//        System.out.println(l3 == l4);
//        System.out.println(i1 == l1);
//        System.out.println(i3 == l3);
//-------------------------------------------------------------------------------------------------------
//        String s = "3";
        String s = " ";
        long num = 0;
//        if(Character.isDigit(s.charAt(0))){
            num = num*10 + s.charAt(0) - '0';
//        }
        System.out.println(num);
        System.out.println("Github使用测试！");
    }

    public static String deleteE(String arr,String element,String regex) {
        String[] splitArr = arr.split(regex);
        List<String> strList = new ArrayList<>();
        Collections.addAll(strList, splitArr);
        boolean contains = strList.contains(element);
        Stream<String> stringStream = null;
        if(contains) {
            stringStream = strList.stream().filter(e -> !e.equals(element));
        }
        String newStr = null;
        if(stringStream != null){
            newStr = stringStream.collect(Collectors.joining(","));
        }else{
            newStr = arr;
        }
        return newStr;
    }
}
