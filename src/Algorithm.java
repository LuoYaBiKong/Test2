import com.singularsys.jep.Jep;
import com.singularsys.jep.JepException;

public class Algorithm {
    public static void main(String[] args) {
        Jep jep = new Jep();
        try {
            jep.addVariable("x", 10);
            jep.parse("(x+1)^2+1");
            Object result = jep.evaluate();
            System.out.println(result);

        } catch (JepException e) {
            System.out.println("An error occurred: " + e.getMessage());
        }
    }
}
